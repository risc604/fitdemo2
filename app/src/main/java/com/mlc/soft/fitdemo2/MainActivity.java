package com.mlc.soft.fitdemo2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getSimpleName();

    TextView    tvMesg;
    Button      btnClean;

    long counters = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.w(TAG, "onCreate(), ");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    protected void onStart() {
        Log.w(TAG, "onStart(), ");
        initView();
        initControl();
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.w(TAG, "onResume(), ");
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.w(TAG, "onStop(), ");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.w(TAG, "onDestroy(), ");
        super.onDestroy();
    }

    @SuppressLint("SetTextI18n")
    public void OnButtonClick(View view)
    {
        Log.w(TAG, "OnButtonClick(), " + view.getId());
        switch (view.getId())
        {
            case R.id.btn_Clean:
                tvMesg.setText("");
                break;

            case R.id.tv_Msg:
                tvMesg.setText("\t Counts: " + (++counters));
                break;

            default:
                break;
        }
    }



    //----------------------- User define function -----------------------------------//
    private void initView()
    {
        Log.w(TAG, " initView(), ");
        tvMesg = findViewById(R.id.tv_Msg);
        btnClean = findViewById(R.id.btn_Clean);
    }

    private void initControl()
    {
        Log.w(TAG, " initControl(), ");

    }


}
